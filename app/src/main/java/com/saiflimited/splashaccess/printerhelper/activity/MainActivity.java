package com.saiflimited.splashaccess.printerhelper.activity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.widget.TextView;
import android.widget.Toast;

import com.saiflimited.splashaccess.R;
import com.saiflimited.splashaccess.printerhelper.utils.AidlUtil;
import com.saiflimited.splashaccess.printerhelper.utils.ESCUtil;
import com.saiflimited.splashaccess.printerhelper.webview.CustomWebView;

public class MainActivity extends AppCompatActivity {

    CustomWebView mWebView;
    TextView noInternetMessage;
    boolean hasInternet = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWebView = (CustomWebView) findViewById(R.id.webview);
        noInternetMessage = (TextView) findViewById(R.id.no_internet_message);
        loadwebView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (hasInternet != isInternetOn()) {
            hasInternet = isInternetOn();
            showHideViewDependingUponInternet(hasInternet);
        }
    }

    private void showHideViewDependingUponInternet(boolean hasInternet) {
        if (hasInternet) {
            noInternetMessage.setVisibility(View.GONE);
            mWebView.setVisibility(View.VISIBLE);
            mWebView.loadUrl("http://www.d01.co.uk/");
        } else {
            noInternetMessage.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }

    private void loadwebView() {
        mWebView.getSettings().setGeolocationEnabled(true);
        //  mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // mWebView.getSettings().setJavaScriptEnabled(true);
        CookieManager.getInstance().setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, true);
        }
        mWebView.getSettings().setJavaScriptEnabled(true);
        //  mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        hasInternet = isInternetOn();
        showHideViewDependingUponInternet(hasInternet);
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        // mWebView.loadUrl("file:///android_asset/index.html");
        mWebView.addJavascriptInterface(new WebViewJavaScriptInterface(this), "app");
    }

    public class WebViewJavaScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message, boolean underline, boolean bold, int imageAlign, int textAlign) {
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            //sendData(BytesUtil.getBaiduTestBytes());

            setAlignment(imageAlign);
            AidlUtil.getInstance().printBitmap(BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.logo_monochrome_with_text));
            setAlignment(textAlign);
            AidlUtil.getInstance().printText(message, 30, bold, underline);

        }

        @JavascriptInterface
        public void makeToast(String message, boolean underline, boolean bold) {
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            //sendData(BytesUtil.getBaiduTestBytes());

            setAlignment(1);
            AidlUtil.getInstance().printBitmap(BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.logo_monochrome_with_text));
            AidlUtil.getInstance().printText(message, 30, bold, underline);

        }
    }

    public void setAlignment(int alignment) {
        switch (alignment) {
            case 1:
                ESCUtil.alignCenter();
                break;
            case 2:
                ESCUtil.alignLeft();
                break;
            case 3:
                ESCUtil.alignRight();
                break;

        }
    }

    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }

}
